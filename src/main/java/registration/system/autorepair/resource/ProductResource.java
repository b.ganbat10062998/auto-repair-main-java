package registration.system.autorepair.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import registration.system.autorepair.dto.ProductRequestDto;
import registration.system.autorepair.service.ProductService;

@RestController
@RequestMapping("product")
@RequiredArgsConstructor
public class ProductResource {
    private final ProductService productService;

    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @GetMapping("list")
    public ResponseEntity<Object> getProductList() {
        return this.productService.getProductList();
    }

    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @PostMapping
    public ResponseEntity<Object> createProduct(@RequestBody ProductRequestDto requestDto) {
        return this.productService.createdProduct(1002L, requestDto);
    }
    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @PostMapping("update")
    public ResponseEntity<Object> updateProductInfo(@RequestBody ProductRequestDto requestDto) {
        return this.productService.updateProduct(1002L, requestDto);
    }
    @CrossOrigin(allowedHeaders = "*", origins = "http://localhost:3000")
    @PostMapping("delete")
    public ResponseEntity<Object> deleteProduct(@RequestParam("productId") Long productId) {
        return this.productService.deleteProductById(productId, 1003L);
    }
}
