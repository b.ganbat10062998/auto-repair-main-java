package registration.system.autorepair.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import registration.system.autorepair.dto.PositionDto;
import registration.system.autorepair.service.PositionService;

@RestController
@RequestMapping("position")
@RequiredArgsConstructor
public class PositionResource {
    private final PositionService positionService;

    @PostMapping()
    public ResponseEntity<?> createPosition(@RequestBody PositionDto request) {
        return this.positionService.createPosition(request);
    }
}
