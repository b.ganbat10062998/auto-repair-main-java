package registration.system.autorepair.enums.converter;

import jakarta.persistence.AttributeConverter;
import registration.system.autorepair.enums.EStaffGender;

import java.util.stream.Stream;

public class StaffGenderConverter implements AttributeConverter<EStaffGender, String> {
    @Override
    public String convertToDatabaseColumn(EStaffGender attribute) {
        if (attribute == null) return null;
        return attribute.getValue();

    }

    @Override
    public EStaffGender convertToEntityAttribute(String dbData) {
        if (dbData == null) return null;
        return Stream.of(EStaffGender.values())
                .filter(i -> i.getValue().equals(dbData))
                .findFirst().orElseThrow();
    }
}
