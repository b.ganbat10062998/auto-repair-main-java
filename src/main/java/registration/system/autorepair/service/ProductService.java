package registration.system.autorepair.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import registration.system.autorepair.dto.MainResponse;
import registration.system.autorepair.dto.ProductListDto;
import registration.system.autorepair.dto.ProductRequestDto;
import registration.system.autorepair.dto.ProductTypeDto;
import registration.system.autorepair.entityManager.EProduct;
import registration.system.autorepair.entityManager.EProductType;
import registration.system.autorepair.repository.ProductRepositoryNative;
import registration.system.autorepair.repository.RProduct;
import registration.system.autorepair.repository.RProductType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static registration.system.autorepair.utils.ConstLocal.IS_ACTIVE_0;
import static registration.system.autorepair.utils.ConstLocal.IS_ACTIVE_1;
import static registration.system.autorepair.utils.MainResponseCode.DELETE_SUCCESS_STR;
import static registration.system.autorepair.utils.MainResponseCode.EXISTED_TYPE_CODE;
import static registration.system.autorepair.utils.MainResponseCode.EXISTED_TYPE_STR;
import static registration.system.autorepair.utils.MainResponseCode.NOT_FOUND_PROD_TYPE_CODE;
import static registration.system.autorepair.utils.MainResponseCode.NOT_FOUND_PROD_TYPE_STR;
import static registration.system.autorepair.utils.MainResponseCode.PRODUCT_NOT_FOUND_CODE;
import static registration.system.autorepair.utils.MainResponseCode.PRODUCT_NOT_FOUND_STR;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_CODE;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_STR;
import static registration.system.autorepair.utils.MainResponseCode.UPDATE_SUCCESS_STR;
import static registration.system.autorepair.utils.MainResponseCode.WRONG_TYPE_CODE;
import static registration.system.autorepair.utils.MainResponseCode.WRONG_TYPE_STR;

@Component
@Service
@RequiredArgsConstructor
@Log4j2
public class ProductService {
    private final RProduct productRepo;
    private final RProductType productTypeRepo;
    private final ProductRepositoryNative productRepositoryNative;

    public ResponseEntity<Object> getProductList() {
        List<ProductListDto> productList = this.productRepositoryNative.getProductList();
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    public ResponseEntity<Object> createdProduct(final Long userId, final ProductRequestDto requestDto) {
        log.info("product create function starting {}", requestDto.getName());
        Optional<EProductType> typeOpt = this.productTypeRepo.findById(requestDto.getTypeId());
        if (typeOpt.isEmpty()) {
            return new ResponseEntity<>(new MainResponse(NOT_FOUND_PROD_TYPE_CODE, NOT_FOUND_PROD_TYPE_STR), HttpStatus.BAD_REQUEST);
        }
        Optional<EProduct> prodOpt = this.productRepo.findByNameAndTypeId(requestDto.getName().toUpperCase(), requestDto.getTypeId());
        if (prodOpt.isPresent()) {
            return new ResponseEntity<>(new MainResponse(WRONG_TYPE_CODE, WRONG_TYPE_STR), HttpStatus.BAD_REQUEST);
        }
        EProduct product = new EProduct();
        product.setActive(IS_ACTIVE_1);
        product.setCreatedDate(LocalDateTime.now());
        product.setName(requestDto.getName());
        product.setImage(requestDto.getImageUrl());
        product.setCreatedUserId(userId);
        product.setQuantity(requestDto.getQuantity());
        product.setTypeId(requestDto.getTypeId());
        this.productRepo.save(product);
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, SUCCESS_STR), HttpStatus.OK);
    }

    public ResponseEntity<Object> updateProduct(final Long userId, final ProductRequestDto requestDto) {
        log.info("product info updated starting ... {}", requestDto.getName());
        Optional<EProduct> productOpt = this.productRepo.findByIdAndActive(requestDto.getProductId(), IS_ACTIVE_1);
        if (productOpt.isEmpty()) {
            return new ResponseEntity<>(new MainResponse(PRODUCT_NOT_FOUND_CODE, PRODUCT_NOT_FOUND_STR), HttpStatus.BAD_REQUEST);
        }
        EProduct product = productOpt.get();
        product.setUpdatedDate(LocalDateTime.now());
        product.setUpdatedUserId(userId);
        product.setName(requestDto.getName());
        product.setImage(requestDto.getImageUrl());
        product.setQuantity(requestDto.getQuantity());
        product.setTypeId(requestDto.getTypeId());
        this.productRepo.save(product);
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, UPDATE_SUCCESS_STR), HttpStatus.OK);
    }

    // delete product by id
    public ResponseEntity<Object> deleteProductById(final Long productId, final Long userId) {
        log.info("product info delete starting ... {} ", productId);
        Optional<EProduct> prodOpt = this.productRepo.findByIdAndActive(productId, IS_ACTIVE_1);
        if (prodOpt.isEmpty()) {
            return new ResponseEntity<>(new MainResponse(PRODUCT_NOT_FOUND_CODE, PRODUCT_NOT_FOUND_STR), HttpStatus.BAD_REQUEST);
        }
        EProduct product = prodOpt.get();
        product.setActive(IS_ACTIVE_0);
        product.setUpdatedDate(LocalDateTime.now());
        product.setUpdatedUserId(userId);
        this.productRepo.save(product);
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, DELETE_SUCCESS_STR), HttpStatus.OK);
    }

    public ResponseEntity<Object> createProductType(final ProductTypeDto request) {
        Optional<EProductType> checkType = this.productTypeRepo.findOneByName(request.getName());
        if (checkType.isPresent()) {
            return new ResponseEntity<>(new MainResponse(EXISTED_TYPE_CODE, EXISTED_TYPE_STR), HttpStatus.BAD_REQUEST);
        }
        this.productTypeRepo.save(new EProductType(request.getName(), request.getDescription(), IS_ACTIVE_1));
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, SUCCESS_STR), HttpStatus.OK);
    }

    public ResponseEntity<?> deleteType(final Long id) {
        Optional<EProductType> checkType = this.productTypeRepo.findById(id);
        if (checkType.isEmpty()) {
            return new ResponseEntity<>(new MainResponse(NOT_FOUND_PROD_TYPE_CODE, NOT_FOUND_PROD_TYPE_STR), HttpStatus.BAD_REQUEST);
        }
        checkType.get().setActive(IS_ACTIVE_0);
        this.productTypeRepo.save(checkType.get());
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, DELETE_SUCCESS_STR), HttpStatus.OK);
    }

    public ResponseEntity<?> getProductTypeList() {
        List<EProductType> list = this.productTypeRepo.findByActive(IS_ACTIVE_1);
        if (!list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
    }

}
