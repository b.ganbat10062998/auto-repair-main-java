package registration.system.autorepair.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import registration.system.autorepair.repository.ProductRepositoryNative;
import registration.system.autorepair.repository.StaffRepositoryNative;

@Service
@RequiredArgsConstructor

public class DashboardService {
    private final StaffRepositoryNative staffRepositoryNative;
    private final ProductRepositoryNative productRepositoryNative;

    public ResponseEntity<?> getDashboardData(){
         return null;
    }
}
