package registration.system.autorepair.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import registration.system.autorepair.dto.MainResponse;
import registration.system.autorepair.dto.ServiceDto;
import registration.system.autorepair.entityManager.EService;
import registration.system.autorepair.repository.RService;

import java.time.LocalDateTime;
import java.util.Optional;

import static registration.system.autorepair.utils.ConstLocal.IS_ACTIVE_1;
import static registration.system.autorepair.utils.MainResponseCode.DUPLICATED_CODE;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_CODE;
import static registration.system.autorepair.utils.MainResponseCode.SUCCESS_STR;

@Component
@Service
@Log4j2
@RequiredArgsConstructor
public class ServiceService {
    private final RService serviceRepository;

    public ResponseEntity<Object> getServiceList() {
        return null;
    }

    public ResponseEntity<Object> createdService(final ServiceDto serviceDto) {
        Optional<EService> checkService = this.serviceRepository.findByCodeAndIsActive(serviceDto.getCode(), IS_ACTIVE_1);
        if (checkService.isPresent()) {
            return new ResponseEntity<>(new MainResponse(DUPLICATED_CODE, "Үйлчилгээний код давхцаж байна."), HttpStatus.BAD_REQUEST);
        }
        EService service = new EService();
        service.setActive(IS_ACTIVE_1);
        service.setName(serviceDto.getName());
        service.setCreatedDate(LocalDateTime.now());
        service.setPrice(serviceDto.getPrice());
        service.setCode(serviceDto.getCode());
        this.serviceRepository.save(service);
        return new ResponseEntity<>(new MainResponse(SUCCESS_CODE, SUCCESS_STR), HttpStatus.OK);
    }
}
