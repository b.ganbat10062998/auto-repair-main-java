package registration.system.autorepair;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"registration.system.autorepair.repository", "registration.system.autorepair.service", "registration.system.autorepair.entity"})
@ComponentScan(basePackages = {"registration.system.autorepair.*"})
@EnableAutoConfiguration
@EnableJpaRepositories({"registration.system.autorepair.*"})
public class AutorepairApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutorepairApplication.class, args);
    }

}
