package registration.system.autorepair.entityManager;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "qpay_invoice")
@Getter
@Setter
@SequenceGenerator(name = "qpay_invoice_id_seq", sequenceName = "qpay_invoice_id_seq", allocationSize = 1)
public class EQpayInvoice implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qpay_invoice_id_seq")
    private Long id;
    @Column(name = "invoice_code")
    private String invoiceCode;
    @Column(name = "is_paid")
    private Short isPaid;
    @Column(name = "paid_date")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime paidDate;
    @Column(name = "order_id")
    private Long orderId;
    @Column(name = "created_user_id")
    private Long createdUserId;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "active")
    private Short active;
}
