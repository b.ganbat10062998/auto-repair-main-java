package registration.system.autorepair.entityManager;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "product")
@SequenceGenerator(sequenceName = "product_id_seq", name = "product_id_seq", allocationSize = 1)
public class EProduct implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_id_seq")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "active")
    private Short active;
    @Column(name = "quantity")
    private BigDecimal quantity;
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime createdDate;
    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime updatedDate;
    @Column(name = "created_user_id")
    private Long createdUserId;
    @Column(name = "updated_user_id")
    private Long updatedUserId;
    @Column(name = "image")
    private String image;
    @Column(name = "type_id")
    private Long typeId;
}
