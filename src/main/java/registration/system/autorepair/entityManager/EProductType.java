package registration.system.autorepair.entityManager;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "product_type")
@Getter
@Setter
@SequenceGenerator(name = "product_type_id_seq", sequenceName = "product_type_id_seq", allocationSize = 1)
@NoArgsConstructor
public class EProductType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_type_id_seq")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "active")
    private Short active;

    public EProductType(String name, String description, Short active) {
        this.name = name;
        this.description = description;
        this.active = active;
    }
}
