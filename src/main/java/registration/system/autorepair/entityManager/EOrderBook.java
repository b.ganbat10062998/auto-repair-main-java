package registration.system.autorepair.entityManager;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "order_book")
@Getter
@Setter
@SequenceGenerator(name = "order_book_id_seq", sequenceName = "order_book_id_seq", allocationSize = 1)
public class EOrderBook implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_book_id_seq")
    private Long id;
    @Column(name = "active")
    private Short active;
    @Column(name = "service_id")
    private Long serviceId;
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime createdDate;
    @Column(name = "total_amount")
    private BigDecimal totalAmount;
    @Column(name = "is_paid")
    private Short isPaid;
    @Column(name = "updated_date")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime updatedDate;
    @Column(name = "updated_user_id")
    private Long updatedUserId;
    @Column(name = "mobile_number")
    private String mobileNumber;
    @Column(name = "client_register")
    private String clientRegister;
    @Column(name = "car_number")
    private String carNumber;
    @Column(name = "car_mark")
    private String carMark;
    @Column(name = "staff_id")
    private Long staffId;
}
