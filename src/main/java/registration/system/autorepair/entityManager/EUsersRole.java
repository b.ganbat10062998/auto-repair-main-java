package registration.system.autorepair.entityManager;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "users_role")
@Getter
@Setter
@SequenceGenerator(name = "users_role_id_seq", sequenceName = "users_role_id_seq", allocationSize = 1)
public class EUsersRole implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_role_id_seq")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "active")
    private Short active;
}
