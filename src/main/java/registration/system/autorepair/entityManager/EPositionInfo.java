package registration.system.autorepair.entityManager;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "position_info")
@SequenceGenerator(name = "position_info_id_seq", sequenceName = "position_info_id_seq", allocationSize = 1)
public class EPositionInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_info_id_seq")
    private Long id;
    @Column(name = "position_name")
    private String positionInfo;
    @Column(name = "salary")
    private BigDecimal salary;
    @Column(name = "active")
    private Short active;
}
