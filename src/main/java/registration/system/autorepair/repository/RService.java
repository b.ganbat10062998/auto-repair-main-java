package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entityManager.EService;

import java.util.Optional;

@Repository
public interface RService extends JpaRepository<EService, Long> {
    Optional<EService> findByCodeAndIsActive(String code, Short active);
}
