package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entityManager.EProductType;

import java.util.List;
import java.util.Optional;

@Repository
public interface RProductType extends JpaRepository<EProductType, Long> {
    Optional<EProductType> findOneByName(String name);

    List<EProductType> findByActive(Short active);


}
