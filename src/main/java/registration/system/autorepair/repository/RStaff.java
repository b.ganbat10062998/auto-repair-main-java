package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entityManager.EStaff;

import java.util.Optional;

@Repository
public interface RStaff extends JpaRepository<EStaff, Long> {
    Optional<EStaff> findByRegisterNumberAndActive(String registerNumber, Short isActive);
}
