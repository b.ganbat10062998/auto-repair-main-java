package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entityManager.EOrderBook;

@Repository
public interface ROrderBook extends JpaRepository<EOrderBook, Long> {
}
