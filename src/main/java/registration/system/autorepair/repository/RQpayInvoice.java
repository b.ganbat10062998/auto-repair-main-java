package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entityManager.EQpayInvoice;

@Repository
public interface RQpayInvoice extends JpaRepository<EQpayInvoice, Long> {
}
