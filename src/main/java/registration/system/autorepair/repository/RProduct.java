package registration.system.autorepair.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import registration.system.autorepair.entityManager.EProduct;

import java.util.Optional;

@Repository
public interface RProduct extends JpaRepository<EProduct, Long> {
    Optional<EProduct> findByNameAndTypeId(String name, Long typeId);

    Optional<EProduct> findByIdAndActive(Long id, Short active);
}
