package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaffListDto {
    private String lastName;
    private String firstName;
    private String register;
    private String mobileNumber;
    private String address;
    private String positionName;
    private Long salary;
    private String username;
    private String isAdmin;
    private String contract;
    private String createdDate;
}
