package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductTypeDto {
    private String name;
    private String description;
}
