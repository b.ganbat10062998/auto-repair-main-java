package registration.system.autorepair.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PositionDto {
    private String positionName;
    private BigDecimal salary;
}
